# day1-Gillian

## Objective: 
1. Understand the meaning of “Agile” in the name of bootcamp: communication and rapid feedback.
2. Learned about training plan for the next month and knew roughly the tasks for each stage.
3. Watched a TED talk "Build a School in the Cloud". The core of this talk is the SOLE (Self-organized Learning Environment),  which is a kind of active learning that is much better than passive learning. This is also one of the learning concepts of the bootcamp.
4. Learned PDCA cycle management: Plan, Do, Check, Action. The use of PDCA in the learning process can ensure the quality of learning.
5. Learned Concept Map. After understanding the relevant concepts, I and my team members completed a concept map about "What is computer?" according to the six steps of drawing a concept map.
6. Learned Focused Conversation Method--ORID: Objective, Reflective, Interpretive, Decisional. Apply ORID to Daily Report, summary or review.

## Reflective: 
I Learned a lot but didn't feel like it worked right away.

## Interpretive: 
Because many concepts are a kind of thinking, the learning effect cannot be shown immediately.

## Decisional: 
First memorize the theories. I can list them in a notebook, and then apply the theories step by step in the future study and work, such as using ORID in the daily newspaper.